# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=plasma-pa
pkgver=5.21.2
pkgrel=0
pkgdesc="Plasma applet for audio volume management using PulseAudio"
# armhf blocked by qt5-qtdeclarative
# s390x blocked by libksysguard
arch="all !armhf !s390x !mips64"
url="https://kde.org/plasma-desktop/"
license="LGPL-2.1-only OR LGPL-3.0-only AND GPL-2.0-only"
depends="pulseaudio kirigami2"
makedepends="
	extra-cmake-modules
	qt5-qtbase-dev
	qt5-qtdeclarative-dev
	kcoreaddons-dev
	kdeclarative-dev
	kdoctools-dev
	kglobalaccel-dev
	knotifications-dev
	ki18n-dev
	plasma-workspace-dev
	pulseaudio-dev
	libcanberra-dev
	"

case "$pkgver" in
	*.90*) _rel=unstable;;
	*) _rel=stable;;
esac
source="https://download.kde.org/$_rel/plasma/$pkgver/plasma-pa-$pkgver.tar.xz"
subpackages="$pkgname-doc $pkgname-lang"

build() {
	cmake -B build \
		-DCMAKE_BUILD_TYPE=None \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DUSE_GCONF=OFF
	cmake --build build
}

check() {
	cd build
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}
sha512sums="68cee6772a97e6fefbbc02da928273dd4fae14ff2c0342a044e6c028192e075b526df1efcae5df516cf306c94677fae86b2e3e5f01a9932cd9e7a941cfe8a9f8  plasma-pa-5.21.2.tar.xz"
